FROM node

USER node

RUN mkdir -p /home/node/

WORKDIR /home/node/

RUN git clone https://gitlab.com/mariusmanea/todomvc.git

WORKDIR /home/node/todomvc

RUN npm install

EXPOSE 3000

CMD [ "npm", "start" ]
